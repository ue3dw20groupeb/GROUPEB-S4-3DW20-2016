<?php
	// Il manque le e de include pour permettre d'inclure le fichier 'inc.twig.php'
	include 'inc.twig.php';
	//on définit les différentes varibales, et on n'oublie pas les ";" pour fermer les varibales
	$template_index = $twig->loadTemplate('index.tpl');

	$n_jours_previsions = 3;

	$ville = "Limoges";

	//~ Clé API
	//~ Si besoin, vous pouvez générer votre propre clé d'API gratuitement, en créant 
	//~ votre propre compte ici : https://home.openweathermap.org/users/sign_up
	$apikey = "10eb2d60d4f267c79acb4814e95bc7dc";
	
	$data_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?APPID='.$apikey.'&q='.$ville.',fr&lang=fr&units=metric&cnt='.$n_jours_previsions;

	$data_contenu = file_get_contents($data_url);
		
	$_data_array = json_decode($data_contenu, true);

	$_ville = $_data_array['city'];
	// Tout le code étéant en anglais, il faut faire attention à l'orthographe des mots, ici liste ne prend pas de "e"
	$_journees_meteo = $_data_array['list'];

	// Cette boucle permet de faire comprendre au navigateur qu'il faut qu'il affiche les informations météo 
	// tant que la varible i est infèrieur à la varible n_jours_prévisions
	for ($i = 0; $i < count($_journees_meteo); $i++) {
		$_meteo = getMeteoImage($_journees_meteo[$i]['weather'][0]['icon']);
		
		$_journees_meteo[$i]['meteo'] = $_meteo;
	}
	
	// Cette stucture permet d'afficher sur le navigateur les variables demandées
	//Pour que l'affiche se fasse correctement, il manquait un appel de la varible ville
	echo $template_index->render(array(
		'_journees_meteo'	=> $_journees_meteo,
		'_ville'			=> $_ville,
		'n_jours_previsions'=> $n_jours_previsions,
		'ville'				=>$ville
	));
	
	// Cette fonction sert à afficher les pictogrammes du temps
	function getMeteoImage($code){
		if(strpos($code, 'n'))
			return 'entypo-moon';
		

		$_icones_meteo =array(
'01d' => 'entypo-light-up',
'02d' => 'entypo-light-up',
// Il manquait la virgule pour indiquer que la liste n'était pas finie
'03d' => 'entypo-cloud',
'04d' => 'entypo-cloud',
'09d' => 'entypo-water', 
'10d' => 'entypo-water',
'11d' => 'entypo-flash',
'13d' => 'entypo-star', 
'50d' => 'entypo-air');
	
		// La boucle if permet de dire au navigateur que si les tableaux code et _icones_météo existe alors il doit les afficher
		// Attention a ne pas oublier d'ouvrir la condition if pour que celle si fonctionne
		if(array_key_exists($code, $_icones_meteo)){
			return $_icones_meteo[$code];
		// sinon il doit afficher le pictogramme entypo-help
		}else{
			return 'entypo-help';
		}
	}
