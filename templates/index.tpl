{# Include sert à indiquer que index.tpl doit inclure le fichier top.tpl #}
{{ include('top.tpl') }}
<div class="preloader">
	<div class="preloader-top">
		<div class="preloader-top-sun">
			<div class="preloader-top-sun-bg"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-0"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-45"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-90"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-135"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-180"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-225"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-270"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-315"></div>
		</div>
	</div>
	<div class="preloader-bottom">
		<div class="preloader-bottom-line preloader-bottom-line-lg"></div>
		<div class="preloader-bottom-line preloader-bottom-line-md"></div>
		<div class="preloader-bottom-line preloader-bottom-line-sm"></div>
		<div class="preloader-bottom-line preloader-bottom-line-xs"></div>
	</div>
</div>
<div class="wrapper">
	<section class="bordure">
	    <p>Météo</p>
	</section>

	<section class="contenu">
	    <h1>
			{# Les doubles crochets servent à appeler des varibles définies, ici on a enlevé le "_"
			de ville pour lui indiquer qu'il doit appeler la variable $ville définie dans le fichier
			index.php #}
	    	{{ville|capitalize}}, {{_ville.country|upper}} 
	    	<a href="http://maps.google.com/maps?q={{_ville.coord.lat}},{{_ville.coord.lon}}" class="lk" target="_blank" title="Voir sur une carte">
	    		Voir sur une carte
	    	</a>
	    </h1>
		{# La fonction for va, ici, permettre d'afficher les informations pour la variable _journees_meteo #}
	    {% for journee in _journees_meteo %}
	    	<div class="jour">
	    		<div class="numero_jour">
					{# On affiche la date #}
	    			<h2>Météo du {{journee.dt|date('d/m/Y')}}</h2>
	    		</div>

			    <div class="temperature {{journee.meteo}}">
					{# On affiche la température pour la date demandée #}
			      <h2>{{journee.temp.day}}<span class="degree-symbol">°</span>C</h2>
			    </div>

			    <ul>
			      <li class="fontawesome-leaf left">
					{# On affiche la vitesse du vent pour la date demandée #}
			        <span>{{journee.speed}} km/h</span>
			      </li>
			      <li class="fontawesome-tint center">
					{# On affiche le taux d'humidité pour la date demandée #}
			        <span>{{journee.humidity}}%</span>
			      </li>
			      <li class="fontawesome-dashboard right">
					{# On affiche la pression de l'air pour la date demandée #}
			        <span>{{journee.pressure}}</span>
			      </li>
			    </ul> 
			    <div class="description">
					{# On affiche une description de la météo pour la date demandée #}
			    	Description : {{journee.weather|first.description|capitalize}}
			    </div>
			</div>
	    {% endfor %}

	    <div class="bullets">
			{# On active la class "bullets" sur le premier élément de la météo #}
	    	{% for i in 1..n_jours_previsions %}
	    		<span class="entypo-record" data-cible="{{i-1}}"></span>
	    	{% endfor %}
	    </div>

	</section>
</div>
{# Il manque le "ud" pour inclure le fichier bottom.tpl dans index.tpl #}
{{ include('bottom.tpl') }}