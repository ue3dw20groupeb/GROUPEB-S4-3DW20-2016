<?php
	// cette fonction permet d'inclure et d'évaluer une seule fois le fichier spécifié 
	include_once('lib/Twig/Autoloader.php');

	Twig_Autoloader::register();
	// le mot clef new permet de créer une nouvelle instance de classe
	$templates = new Twig_Loader_Filesystem('templates');
	// Il manque un e à new ce qui empêche de créer la nouvelle instance de classe : templates
	$twig      = new Twig_Environment($templates); 
